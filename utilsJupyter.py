import json

import requests

PAGE_SIZE = 100

#this module loads statements from a json file and saves results to a json file instead of accessing the stupla x api

def get_statements_page(api_url, analytics_token, last_object_id=None):
    print(api_url)
    payload = (
        {"page_size": PAGE_SIZE, "last_object_id": last_object_id}
        if last_object_id
        else {"page_size": PAGE_SIZE}
    )
    response = requests.post(
        f"{api_url}/api/v1/provider/data",
        headers={"Authorization": f"Basic {analytics_token}"},
        json=payload,
    )
    response.raise_for_status()

    payload = response.json()
    return payload.get("statements", [])


def get_statements(api_url, analytics_token):
    #reads data from data/stupla_x_api_statements.json
    statements = []
    with open('data/stupla_x_api_statements.json') as json_file:
        data = json.load(json_file)
        statements.extend(data)
    return statements

def get_existing_results(api_url, analytics_token):
    response = requests.get(
        f"{api_url}/api/v1/provider/results",
        headers={"Authorization": f"Basic {analytics_token}"},
    )
    response.raise_for_status()
    data = response.json()
    return data

def save_results(api_url, analytics_token, results):
    #save results to file in data/results.json
    with open('results/results.json', 'w') as outfile:
        json.dump(results, outfile)
