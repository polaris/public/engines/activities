import sys
import time

from requests import HTTPError

from utils import get_statements, save_results


def main(analytics_token, api_url):
    try:
        # Retrieve available statements
        statemens = get_statements(api_url, analytics_token)

        #count every type of 'verb_short' in the statements list
        verbs = {}
        for statement in statemens:
            verb = statement["verb_short"]
            if verb in verbs:
                verbs[verb] += 1
            else:
                verbs[verb] = 1

        description = {
            "de": "Anzahl Statements",
            "en": "Number of statements",
        }

        # Send result to rights engine
        result_time = int(time.time())
        save_results(
            api_url,
            analytics_token,
            {'time' : result_time,"result": verbs, "description": description},
        )
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])